/*
 * Sample program for DokodeModem
 * RTC set/display demo
 * Copyright (c) 2024 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <dokodemo.h>

DOKODEMO Dm = DOKODEMO();

void disp_rtc()
{
  DateTime tm = Dm.rtcNow();
  char buf[64];
  snprintf(buf, sizeof(buf), "%d,%02d,%02d,%02d,%02d,%02d\r\n", tm.year(), tm.month(), tm.day(), tm.hour(), tm.minute(), tm.second());
  SerialDebug.print(buf);
}

void setup()
{
  Dm.begin(); // 初期化が必要です。

  // デバッグ用　USB-Uart
  SerialDebug.begin(115200);

  //RTC 初期化
  Dm.rtcAdjust(DateTime(2024, 4, 1, 12, 0, 0));
}

void loop()
{
  disp_rtc();
  delay(10000);
}
